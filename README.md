# Ansible Role: Go

[![Build Status](https://travis-ci.org/halberom/ansible-role-go.svg?branch=master)](https://travis-ci.org/halberom/ansible-role-go.svg)

Installs Golang and a Go Application on Debian/Ubuntu linux servers.

This role installs and configures Golang, and then gets (builds and installs) a Go application from a Git repository.  It installs and configures Supervisor to run the application as a daemon.

## Requirements

None.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    go_user: "go"

The user the application files will be owned by, and the app will be run as.

    go_group: "go"

The user's primary group

    go_groups:
      - www-data

A list of additional groups to add the user to

    go_packages:
      - git
      - golang

The packages to be installed as dependencies

    go_app_name: ""

The name of the go application to be installed

    go_app_repo_path: ""

The repository URL of the Go application, should be in the format expected by ```go get```, e.g. ```github.com/halberom/goapp```

    go_command: "go"

The command to run a go application with

    go_path: "/opt/go"

The workspace location, used in the environment variable GOPATH

    go_bin_path: "{{ go_path }}/bin"

The location of built applications

    go_environment:
      GOPATH: "{{ go_path }}"
      GOBIN: "{{ go_bin_path }}"

Environment variables used when running ```go``` commands, and added to the supervisor config

## Dependencies

None.

## Example Playbook

    - hosts: server
      roles:
        - { role: halberom.go }

## License

MIT / BSD

## Author Information

This role was created in 2016 by [Gerard Lynch](http://halberom.com/)
